using System;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;


namespace BinaryParkingClientServerNiverovskyiConsole
{
    public class ParkingManager
    {
        private static string GET(string method)
        {
            try
            {
                using (var client = new WebClient())
                {
                    return client.DownloadString("https://localhost:5001/api/" + method);
                }
            }
            catch (WebException)
            {
                return "Error connection!((";
            }
        }

        public static void Start()
        {
            Console.WriteLine("Hello! I'm ParkingManager3000. I'll help to manage parking.\n" +
                              "Do you want to configure the program? (Press Y/y if you want, else the default settings will be left)");
            var ans = '\0';
            try
            {
                ans = Convert.ToChar(Console.ReadLine());
            }
            catch (FormatException)
            {
            }

            if (ans == 'Y' || ans == 'y')
                Settings();
            Menu();
        }

        private static void Menu()
        {
            while (true)
            {
                Console.WriteLine("What do you want to do?\n " +
                                  "1. - Find out the current Parking balance\n " +
                                  "2. - The amount of money earned in the last minute\n " +
                                  "3. – Find out the number of free / occupied parking places.\n " +
                                  "4. - Display all Parking Transactions for the last minute\n " +
                                  "5. – Print the entire history of transactions (reading data from the file Transactions.log).\n " +
                                  "6. - Display a list of all Vehicles.\n " +
                                  "7. - Add Vehicle to the Parking.\n " +
                                  "8. – Remove Vehicle from the Parking.\n " +
                                  "9. – Recharge the balance of a particular Vehicle.\n " +
                                  "0. – Exit the best application ParkingManager3000((( ");
                try
                {
                    switch (Convert.ToInt32(Console.ReadLine()))
                    {
                        case 1:
                            Console.WriteLine($"The Parking balance: {GET("parking/Balance")}");
                            break;
                        case 2:
                            Console.WriteLine($"Amount of money: {GET("transaction/MinuteSumOfIncome")}");
                            break;
                        case 3:
                            ParkingPlaces();
                            break;
                        case 4:
                            PrintMinuteTransactions();
                            break;
                        case 5:
                            PrintAllTransactions();
                            break;
                        case 6:
                            WriteAllVehicles();
                            break;
                        case 7:
                            AddVehicleToParking();
                            break;
                        case 8:
                            RemoveVehicleFromParking();
                            break;
                        case 9:
                            RechargeBalanceOfVehicle();
                            break;
                        case 0:
                            Console.WriteLine("It is very sad to say goodbye to you (((\nBut goodbye");
                            GET("parking/Quit");
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Input number was not correct.");
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format.");
                }
            }
        }

        private static void RechargeBalanceOfVehicle()
        {
            if (Convert.ToInt32(GET("parking/PlacesEngaged")) == 0)
            {
                Console.WriteLine("Empty!");
                return;
            }
            Console.WriteLine("Which vehicle?");
            WriteAllVehicles();
            var i = -1;
            do
            {
                try
                {
                    i = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (i > Convert.ToInt32(GET("parking/MaxId")) || i < Convert.ToInt32(GET("parking/MinId")));

            Console.WriteLine("Enter the amount of money to supplement: ");
            var amount = -1;
            while (amount < 0)
            {
                try
                {
                    amount = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            }

            GET($"parking/RechargeBalance?id={i}&sum={amount}");
        }

        private static void RemoveVehicleFromParking()
        {
            if (Convert.ToInt32(GET("parking/PlacesEngaged")) == 0)
            {
                Console.WriteLine("Empty!");
                return;
            }
            Console.WriteLine("Which vehicle?");
            WriteAllVehicles();
            var i = -1;
            do
            {
                try
                {
                    i = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (i > Convert.ToInt32(GET("parking/MaxId")) || i < Convert.ToInt32(GET("parking/MinId")));

            GET("parking/Remove?id=" + i);
            Console.WriteLine("Removed successfully");
        }

        private static void AddVehicleToParking()
        {
            Console.WriteLine("Enter type's number of vehicle:\nCar: 0, Bus: 1, Motorcycle: 2, Truck: 3");
            var type = -1;
            while (!(type == 0 || type == 1 || type == 2 || type == 3))
            {
                try
                {
                    type = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need integer");
                }
            }

            Console.WriteLine("Enter balance of car: ");
            var balance = -1;
            while (balance <= 0)
            {
                try
                {
                    balance = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need integer");
                }
            }

            GET($"parking/Add?type={type}&balance={balance}");
        }

        private static void WriteAllVehicles()
        {
            var vehicles = JsonConvert.DeserializeObject(GET("parking/GetAllVehicles"));
            Console.WriteLine(vehicles);
        }


        private static void PrintAllTransactions()
        {
            Console.WriteLine(GET("transaction/GetTransactions"));
        }

        private static void PrintMinuteTransactions()
        {
            var transactions = JsonConvert.DeserializeObject(GET("transaction/GetMinuteTransactions"));
            Console.WriteLine(transactions);
        }

        private static void ParkingPlaces()
        {
            Console.WriteLine($"Free: {GET("parking/PlacesFree")}, Engaged:{GET("parking/PlacesEngaged")}");
        }

        private static void Settings()
        {
            Console.WriteLine(
                "1. Enter initial Parking Balance.");
            double Balance = -1;
            do
            {
                try
                {
                    Balance = Convert.ToDouble(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format.");
                }
            } while (Balance < 0);


            Console.WriteLine("2. Enter maximum Parking Capacity");
            uint MaxCapacity = 0;
            do
            {
                try
                {
                    MaxCapacity = Convert.ToUInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (MaxCapacity <= 0);

            Console.WriteLine(
                "3. Enter the frequency (in seconds) with which the vehicles money will decrease by N-time parking");
            uint PaymentPeriod = 0;
            do
            {
                try
                {
                    PaymentPeriod = Convert.ToUInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (PaymentPeriod <= 0);

            Console.WriteLine("4. Enter tariffs determining how much money Vehicles must pay for Parking N-time");
            double busTariff = -1;
            double passengerCarTariff = -1;
            double motorcycleTariff = -1;
            double truckTariff = -1;
            do
            {
                try
                {
                    Console.WriteLine("Bus:");
                    busTariff = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Passenger car:");
                    passengerCarTariff = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Truck:");
                    motorcycleTariff = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Motorcycle:");
                    truckTariff = Convert.ToDouble(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need a number greater than zero ");
                }
            } while (truckTariff < 0 || motorcycleTariff < 0 ||
                     passengerCarTariff < 0 ||
                     busTariff < 0);

            Console.WriteLine(
                "5. Enter the coefficient of the fine, which will be charged from vehicles if it does not have enough money to pay for parking");
            double penaltyRatio = -1;
            do
            {
                try
                {
                    penaltyRatio = Convert.ToDouble(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine(
                        "Input string was not in a correct format. Need a number greater than or equal to zero ");
                }
            } while (penaltyRatio < 0);

            GET(
                $"parking/EditSettings?balance={Balance}&paymentperiod={PaymentPeriod}&penalty={penaltyRatio}&maxc=" +
                $"{MaxCapacity}&bust={busTariff}&truckt={truckTariff}&cart={passengerCarTariff}&motot={motorcycleTariff}");
        }
    }
}